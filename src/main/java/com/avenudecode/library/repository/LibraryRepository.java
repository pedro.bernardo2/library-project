package com.avenudecode.library.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.avenudecode.library.entity.Book;

@Repository
public interface LibraryRepository extends MongoRepository<Book, String>{
	
	@Query("{ $or: [{ rented : { $exists:false } }, { rented : false }]}")
	public List<Book> getBooksNotRented();

}
