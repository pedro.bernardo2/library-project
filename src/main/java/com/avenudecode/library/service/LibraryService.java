package com.avenudecode.library.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.avenudecode.library.entity.Book;
import com.avenudecode.library.repository.LibraryRepository;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(propagation = Propagation.SUPPORTS)
@RequiredArgsConstructor
public class LibraryService {

	@Autowired
	private LibraryRepository libraryRepository;

	public List<Book> getAll() {
		return libraryRepository.findAll();
	}

	public Book save(Book book) {
		return libraryRepository.save(book);
	}

	public Book updateBook(Book book) {
		Book updateBook = libraryRepository.findById(book.getId()).orElseThrow(
				() -> new RuntimeException(String.format("Não foi possivel encontrar o livro")));
		
		updateBook.setAuthor(book.getAuthor());
		updateBook.setTitle(book.getTitle());
		updateBook.setRented(book.getRented());
		
		return libraryRepository.save(book);
		
	}
	
	public void deleteBook(String id) {
		Book deleteBook = libraryRepository.findById(id).orElseThrow(
				() -> new RuntimeException(String.format("Não foi possivel encontrar o livro")));
		
		libraryRepository.delete(deleteBook);
		
	}
	
	public Book getBookById(String id) {
		return libraryRepository.findById(id).orElseThrow(
				() -> new RuntimeException(String.format("Não foi possivel encontrar o livro")));
	}
	
	public List<Book> getBooksNotRented() {
		return libraryRepository.getBooksNotRented();
	}

}
