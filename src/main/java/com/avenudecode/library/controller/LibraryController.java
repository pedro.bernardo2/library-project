package com.avenudecode.library.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avenudecode.library.entity.Book;
import com.avenudecode.library.service.LibraryService;

@RestController
@RequestMapping("/library")
public class LibraryController {
	
	@Autowired
	private LibraryService libraryService;
	
	@GetMapping
	public List<Book> getAll() {
		return libraryService.getAll();
	} 
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Book saveBook(@RequestBody Book book) {
		return libraryService.save(book);				
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Book updateBook(@RequestBody Book book) {
		return libraryService.updateBook(book);
	}
	
	@DeleteMapping("/{id}")
	public void deleteBook(@PathVariable(value = "id") String id) {
		libraryService.deleteBook(id);
	}
	
	@GetMapping("/{id}")
	public Book getBookById(@PathVariable(value = "id") String id) {
		return libraryService.getBookById(id);
	
	}
	
	@GetMapping("/booksnotrented")
	public List<Book> getBooksNotRented() {
		return libraryService.getBooksNotRented();
	}
	
}