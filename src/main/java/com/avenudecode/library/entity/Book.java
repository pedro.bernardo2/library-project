package com.avenudecode.library.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class Book {
	
	@Id
	private String id;
	
	private String title;
	
	private String author;
	
	private Boolean rented;	

}
